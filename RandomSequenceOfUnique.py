#!/usr/bin/python3

# Create a random sequence of unique integers based on
# https://github.com/preshing/RandomSequence/blob/master/randomsequence.h

class RandomSequenceOfUnique:
	
	__m_index = 0;
	__m_intermediateOffset = 0;
	
	def __overflow32(self, x: int) -> int:
		return x & 0xFFFFFFFF
	
	def __overflow64(self, x: int) -> int:
		return x & 0xFFFFFFFFFFFFFFFF
	
	def __permuteQPR(self, x : int) -> int:
		x = self.__overflow32(x)
		prime = 4294967291
		if x >= prime:
			return x
		residue = self.__overflow64(x * x) % prime
		if x <= int(prime / 2):
			return residue
		else:
			return prime - residue
	
	def __init__(self, seedBase: int, seedOffset: int):
		self.__m_index = self.__permuteQPR(self.__permuteQPR(seedBase) + 0x682f0161)
		self.__m_intermediateOffset = self.__permuteQPR(self.__permuteQPR(seedOffset) + 0x46790905)
	
	def next(self):
		i = self.__permuteQPR((self.__permuteQPR(self.__m_index) + self.__m_intermediateOffset) ^ 0x5bf03635)
		self.__m_index = self.__overflow32(self.__m_index + 1)
		return i

if __name__ == '__main__':
	r = RandomSequenceOfUnique(0, 1)
	for x in range(0, 20):
		i = r.next()
		print(hex(i), i)
